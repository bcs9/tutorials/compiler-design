# Overview

What is a Compiler?

A compiler is at heart a language to language translator.

That's it really.

It is also a series of decodings, transformations and then re-encodings of information until you arrive at another language.

## What Credentials Do You Have?

Although I am young in age and have roughly only 7~ years of programming experience I have spent about 3 years experimenting with writing compilers.

I don't usually publish my work because so often I have to restart from finding that there are more efficient ways to write compilers than the last project.

Because of that there really isn't much to see in terms of a complete compiler.

But recently I have been finding less and less ways to refine my style and have decided to document my knowledge.

Hence this tutorial.

## Why Write A Compiler?

Although it might not seen like there are many good reasons to write a compiler there are actaully many benefits to writing one.

Namely:

1. Understanding Of How Computers Function Internally
2. Understanding Of How Programming Languages Really Function
3. An Opportunity To Innovate

## Don't You Need Complicated Programs Like Flex, Yacc, Bison or ANTLR?

No.

Compilers are just regular programs that transform data.

In fact if you think about it, all computers do is tranform data from one format to another.

I argue that it is actually easier to write the whole compiler front end from scratch than to use a lexer / parser generator.

## Is This Tutorial Mostly Your Opinions?

Yes.

There are some other people's opinions too.

Those will be documented.

Definitions are also written in my understanding, not formal ones.

## Ready?

Chapters:

1. TODO

Enjoy!