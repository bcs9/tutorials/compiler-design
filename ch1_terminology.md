# Basic Terminology

There are a few things that you should know about before you start reading.

Namely, what are:

- Stages
- Loaders
- Extractors
- Lexers
- Parsers
- Abstracters
- Intermediate Representations
- Optimization Levels
- Front Ends
- Back Ends
- Assembly Languages
- ELF / EXE Files

## Disclaimer

Some of these concepts you won't find in traditional tutorials.

This is a style of writing compilers based on my experience.

So if you look up a certain concept and don't find it, that's why.

## Stages

A stage of a compiler is a part of the translation process from one format & langauge to another.

## Loaders

A Loader is a stage of a compiler that is responsible for loading files into one large buffer to be read by other stages.

## Extracters

An Extractor is a stage of a compiler that turns utf-8 strings from the Loader into a list of unicode code points.

## Lexers

A Lexer is a stage of a compiler that takes the unicode code points and groups the characters into what are called lexemes.

Imagine that you had a sentence in English.

That sentence is made up of words and punctuations which are made up of characters.

Lexing groups those characters into logical sections based on a predefined set of rules and marks the type of group.

Take the sentence `The water is blue.`.

You would Lex the sentence like this.

| Types | Meanings |
| ----- | -------- |
| Word  | The      |
| Word  | water    |
| Word  | is       |
| Word  | blue     |
| Punc. | .        |

## Parsers

TODO